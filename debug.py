import os.path as path
from torchutils.visualizers.csv_metrics_visualizer import analyze


EXPROOT = path.join("~", "temp", "experiments")
analyze(exproot=path.join(EXPROOT, "cifar-10"), run_name="late-bush-1635")