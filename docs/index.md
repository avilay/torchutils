# Overview
I ❤️ PyTorch. But it does get a bit cumbersome when I want to do some quick experiments, especially when I want to log a bunch of metrics and the hyperparameters used with each training run. There are other projects that make working with PyTorch as "easy" as working with Keras, but they didn't hit the sweet spot of balancing flexibility vs. ease of use, at least not for me. In addition to this balance, I was also looking for some tensorboard alternatives that made it easy to analyze learning curves and compare training runs inside of Jupyter notebooks. These were my main motivations for creating `torchutils`.

Current version of `torchutils.0.1.0`.

## Installation
Simple pip based installation -

`pip install avilabs-torchutils`

If you plan on using the `csv_metrics_visualizer` also install -

`pip install termcolor`

If you plan on using the MLFlow logger also install -

`pip install mlflow`

## Quickstart
Here is a simple example to run a regression training. See the [Tutorials](##Tutorials) section for more in-depth examples.

```python
from dataclasses import dataclass

import torch as t
from torch.utils.data import DataLoader

from sklearn.metrics import mean_absolute_error

from torchutils import Hyperparams, Trainer, TrainerArgs, evaluate
import torchutils.dataget as datagen
from torchutils.ml_loggers.stdout_logger import StdoutMLExperiment


class Regression(t.nn.Module):
    def __init__(self):
        super().__init__()
        self.fc1 = t.nn.Linear(5, 3)
        self.fc2 = t.nn.Linear(3, 1)

    def forward(self, batch_x):
        x = t.nn.functional.relu(self.fc1(batch_x))
        batch_y_hat = self.fc2(x)
        return t.squeeze(batch_y_hat, dim=1)


@dataclass
class MyHyperparams(Hyperparams):
    batch_size: int
    n_epochs: int
    lr: float


def build_trainer(hparams, trainset, valset):
    run_name = "run-" + str(uuid.uuid4())[:8]
    model = Regressor()
    optim = t.optim.Adam(model.parameters(), lr=hparams.lr)
    loss_fn = t.nn.MSELoss(reduction="mean")
    traindl = DataLoader(trainset, batch_size=hparams.batch_size, shuffle=True)
    valdl = DataLoader(valset, batch_size=5000)
    return TrainerArgs(
        run_name=run_name,
        model=model,
        optimizer=optim,
        loss_fn=loss_fn,
        trainloader=traindl,
        valloader=valdl,
        n_epochs=hparams.n_epochs,
    )


trainset, valset, testset = datagen.regression(n_samples=100000)
exp = StdoutMLExperiment("regress-exp")
hparams = MyHyperparams(batch_size=16, n_epochs=10, lr=0.03)
trainer = Trainer(exp, trainset, valset, metric_functions=[mean_absolute_error])
trainer.train(hparams, build_trainer)

testdl = DataLoader(testset, batch_size=5000)
test_metrics = evaluate(trainer.model, testdl, [mean_absolute_error])
```

## Concepts
A **training run** consists of running the forward and backward passes for multiple epochs over the training dataset. Typically a training run is configured using a set of hyperparameters. Multiple related training runs can be grouped together in a single **experiment**. There is no hard rule that tells us which training runs should be grouped in a single experiment. My heuristic is to group those training runs that I would want to compare in the same experiment. This usually means that as long as the model architecture, training setup, number of hyperparameters, etc. remain more or less the same, then they belong in the same experiment.

The metrics for each training run are logged. The user decides which logger to use. `torchutils` comes with a couple of built-in loggers, e.g., loggers to log to stdout, csv files, MLFlow, etc. It is fairly straightforward to implement a new logger, e.g., for logging to Tensorboard, S3, SQLite, etc.

`torchutils` also provides a convenience wrapper on top of [ax.dev](https://ax.dev). `torchutils` uses the Bayesian optimizer to tune the hyperparameters based on a user provided objective function. For now this is implemented in a sequential manner, where objective function results of one training run (consisting of multiple epochs) are used to decide the hyperparameter configuration for the next training run.


## Tutorials
Each tutorial demonstrates the various concepts described above.

  * [Binary Classification using `stdout` Logger](https://gitlab.com/avilay/torchutils/-/blob/0.0.0/examples/binary_classification_stdout_logger.ipynb)
  * [Multiclass Classification using CSV Logger](https://gitlab.com/avilay/torchutils/-/blob/0.0.0/examples/multiclass_classification_csv_logger.ipynb)
  * [Regression using `stdout` Logger](https://gitlab.com/avilay/torchutils/-/blob/0.0.0/examples/binary_classification_stdout_logger.ipynb)
  * [Transfer Learning](https://gitlab.com/avilay/torchutils/-/blob/0.0.0/examples/transfer_learning.ipynb)
  * [Hyper Parameter Tuning of a Binary Classifier](https://gitlab.com/avilay/torchutils/-/blob/0.0.0/examples/tune_binary_classification.ipynb)
  * [Plotting Metrics Logged by CSV Logger](https://gitlab.com/avilay/torchutils/-/blob/0.0.0/examples/transfer_learning.ipynb)
  * [Plotting Parameter Histograms Logged by CSV Logger](https://gitlab.com/avilay/torchutils/-/blob/0.0.0/examples/plot_params.ipynb)

## Reference
The API reference.

  * [evaluator](/torchutils/api/evaluator/)
  * [hyperparams](/torchutils/api/hyperparams)
  * [ml_logger](/torchutils/api/ml_logger)
  * [trainer](/torchutils/api/trainer)
  * [tuner](/torchutils/api/tuner)
  * [ml_loggers/csv_logger](/torchutils/api/ml_loggers/csv_logger)
  * [ml_loggers/stdout_logger](/torchutils/api/ml_loggers/stdout_logger)
  * [visualizers/csv_metric_visualizer](/torchutils/api/visualizer/csv_metric_visualizer)
