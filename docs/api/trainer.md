# trainer

::: torchutils.trainer
    handler: python
    selection:
      members:
        - TrainerArgs
        - Trainer
    rendering:
      show_root_heading: false
      show_source: true
