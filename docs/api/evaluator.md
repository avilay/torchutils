# evaluator

::: torchutils.evaluator
    handler: python
    selection:
      members:
        - evaluate
    rendering:
      show_root_heading: false
      show_source: true
