# stdout_logger

::: torchutils.ml_loggers.stdout_logger
    handler: python
    selection:
      members:
        - StdoutMLRun
        - StdoutMLExperiment
    rendering:
      show_root_heading: false
      show_source: true